import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class ReadingPageController {
    @FXML
    private ListView<String> listView;

    @FXML
    private Label titleLabel;

    @FXML
    private Label countLabel;

    @FXML
    private HBox updateArea;

    private ObservableList<String> observableList;
    private HashMap<String, String> titleToNcodeList;
    private ArrayList<String> subTitleList;
    private ArrayList<String> fileList;

    private File file;
    private FileReader fileReader;
    private BufferedReader bufferedReader;
    private String title;
    private String line;

    private boolean flag;
    private String ncode;

    private NovelPage np;

    private NovelProperty novelProperty;

    @FXML
    void initialize() throws IOException {
        observableList = FXCollections.observableArrayList();
        titleToNcodeList = new HashMap<>();
        subTitleList = new ArrayList<>();
        fileList = new ArrayList<>();
        file = new File("./Novels/titles.txt");
        fileReader = new FileReader(file);
        bufferedReader = new BufferedReader(fileReader);
        title = null;
        line = null;
        flag = false;
        ncode = null;
        np = null;
        novelProperty = new NovelProperty();
        while ((line = bufferedReader.readLine()) != null) {
            String split[] = line.split("/,/");
            titleToNcodeList.put(split[0], split[1]);
        }
        titleLabel.setText("タイトルリスト");
        countLabel.setText("" + titleToNcodeList.size());
        observableList.addAll(titleToNcodeList.keySet());
        listView.setItems(observableList);
        updateArea.visibleProperty().setValue(false);
        updateArea.setManaged(false);
    }

    @FXML
    public void backButtonAction(ActionEvent event) {
        if (flag) {
            flag = false;
            observableList = FXCollections.observableArrayList();
            titleLabel.setText("タイトルリスト");
            countLabel.setText("" + titleToNcodeList.size());
            observableList.addAll(titleToNcodeList.keySet());
            listView.setItems(observableList);
            updateArea.visibleProperty().setValue(false);
            updateArea.setManaged(false);
            System.out.println("Back to title list.");
        } else {
            System.out.println("This is title list.");
        }
    }

    @FXML
    public void mouseClickedAction(MouseEvent event) throws IOException {
        if (flag) {
            if (ncode != null) {
                title = listView.getSelectionModel().getSelectedItem();
                np = new NovelPage(title, ncode, subTitleList, fileList, novelProperty);
                novelProperty = np.showNewWindow();
            } else {
                System.err.println("Cannot find");
            }
        } else {
            title = listView.getSelectionModel().getSelectedItem();
            if (title != null) {
                observableList = FXCollections.observableArrayList();
                flag = true;
                fileList = new ArrayList<>();
                titleLabel.setText(title);
                ncode = titleToNcodeList.get(title);
                file = new File("./Novels/" + ncode + "/" + FileNames.SUBTITLES + ".txt");
                fileReader = new FileReader(file);
                bufferedReader = new BufferedReader(fileReader);
                while ((line = bufferedReader.readLine()) != null) {
                    String splits[] = line.split("/,/");
                    fileList.add(splits[0]);
                    subTitleList.add(splits[1]);
                }
                countLabel.setText("" + (subTitleList.size()));
                observableList.addAll(subTitleList);
                listView.setItems(observableList);
                updateArea.visibleProperty().setValue(true);
                updateArea.setManaged(true);
                System.out.println("Change to subTitle list of <" + title + ">.");
            }
        }
    }

    @FXML
    public void updateButtonAction(ActionEvent event) throws IOException {
        URL url = new URL("https://api.syosetu.com/novelapi/api/?of=nu&ncode=" + ncode);
        HttpURLConnection http = (HttpURLConnection)url.openConnection();
        http.setRequestMethod("GET");
        http.connect();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(http.getInputStream()));
        String updatedAt = null;
        while ((line = bufferedReader.readLine()) != null) {
            updatedAt = line;
        }
        file = new File("./Novels/" + ncode + "/" + FileNames.LASTUPDATE + ".txt");
        bufferedReader = new BufferedReader(new FileReader(file));
        String lastUpdate = null;
        while ((line = bufferedReader.readLine()) != null) {
            lastUpdate = line;
        }
        if (updatedAt.compareTo(lastUpdate) > 0) {
            update();
        }
        else {
            System.out.println("It is the latest version.");
        }
    }

    public void update() throws IOException {
        DLModel dlModel = new DLModel();
        String url = "https://ncode.syosetu.com/" + ncode + "/";
        Document document = Jsoup.connect(url).get();
        dlModel.printSubTitle(ncode, document);
        dlModel.printLastUpdate(ncode);
        ArrayList<String> urlList = dlModel.makeUrlList(document);
        for (String novelUrl: urlList) {
            document = Jsoup.connect( "https://ncode.syosetu.com/" + ncode + "/" + novelUrl + "/").get();
            dlModel.printNovel(novelUrl, ncode, document);
        }
        System.out.println("update complete.");
    }

    @FXML
    public void backToTopButtonAction(ActionEvent event) throws IOException {
        App.getInstance().setPage("TopPage.fxml");
        System.out.println("Back to top page.");
    }
}
