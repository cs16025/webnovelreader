public class NovelProperty {
    private boolean isNightMode;
    private int fontSize;

    public NovelProperty() {
        isNightMode = false;
        fontSize = 10;
    }

    public NovelProperty(boolean isNightMode, int fontSize) {
        this.isNightMode = isNightMode;
        this.fontSize = fontSize;
    }

    public boolean getNightMode() {
        return isNightMode;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setNightMode(boolean isNightMode) {
        this.isNightMode = isNightMode;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }
}
