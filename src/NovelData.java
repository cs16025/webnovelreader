import java.util.ArrayList;

public class NovelData {
    private String title;
    private String url;
    private ArrayList<String> subTitleList;

    public NovelData(String title, String url) {
        this.title = title;
        this.url = url;
        subTitleList = new ArrayList<>();
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void addSubTitleList(String subList) {
        subTitleList.add(subList);
    }
}
