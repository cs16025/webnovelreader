import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class App extends Application {
    private static App singleton;

    private Stage stage;
    private Parent root;
    private FXMLLoader fxmlLoader;

    public void start(Stage primaryStage) throws IOException {
        singleton = this;

        fxmlLoader = new FXMLLoader(getClass().getResource("TopPage.fxml"));
        root = fxmlLoader.load();
        stage = primaryStage;
        stage.setTitle("web小説リーダー");
        stage.setScene(new Scene(root));
        stage.show();
        System.out.println("Show top page.");
    }

    public void setPage(String fxml) throws IOException {
        fxmlLoader = new FXMLLoader(getClass().getResource(fxml));
        root = fxmlLoader.load();
        stage.setScene(new Scene(root));
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static App getInstance() {
        return singleton;
    }
}
