import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class DLModel {
    private ArrayList<DLData> dlList;

    public DLModel() {
        dlList = new ArrayList<>();
    }

    public void addIndex(String title, String url) {
        dlList.add(new DLData(title, url));
    }

    public void format() throws IOException {
        System.out.println("Formatting start.");
        for (DLData dlData : dlList) {
            String url = dlData.getUrl();
            Document document = Jsoup.connect(url).get();
            String split[] = url.split("/");
            String ncode = split[3];
            makeDirectory(ncode);
            printTilte(ncode, dlData.getTitle());
            printSubTitle(ncode, document);
            printLastUpdate(ncode);
            ArrayList<String> urlList = makeUrlList(document);
            for (String novelUrl: urlList) {
                document = Jsoup.connect( "https://ncode.syosetu.com/" + ncode + "/" + novelUrl + "/").get();
                printNovel(novelUrl, ncode, document);
            }
        }
        addTitles();
        System.out.println("Formatting complete.");
    }

    public void makeDirectory(String ncode) {
        File newDir = new File("./Novels/" + ncode);
        newDir.mkdir();
        System.out.println("Make novel directory");
    }

    public void printTilte(String ncode, String title) throws IOException {
        File file = new File("./Novels/" + ncode + "/" + FileNames.TITLE + ".txt");
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
        bufferedWriter.write(title);
        bufferedWriter.close();
        System.out.println("Make title file.");
    }

    public void printSubTitle(String ncode, Document document) throws IOException {
        Elements elements = document.getElementsByClass("subtitle");
        List<String> subTitles = elements.eachText();
        ArrayList<String> urlList = makeUrlList(document);
        File file = new File("./Novels/" + ncode + "/" + FileNames.SUBTITLES + ".txt");
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
        for (int i = 0; i < subTitles.size() && i < urlList.size(); i++) {
            bufferedWriter.write(urlList.get(i) + "/,/" + subTitles.get(i));
            bufferedWriter.newLine();
        }
        bufferedWriter.close();
        System.out.println("Make subtitle file.");
    }

    public void printLastUpdate(String ncode) throws IOException {
        URL url = new URL("https://api.syosetu.com/novelapi/api/?of=nu&ncode=" + ncode);
        HttpURLConnection http = (HttpURLConnection)url.openConnection();
        http.setRequestMethod("GET");
        http.connect();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(http.getInputStream()));
        String line, lastUpdate = null;
        while ((line = bufferedReader.readLine()) != null) {
            lastUpdate = line;
        }
        File file = new File("./Novels/" + ncode + "/" + FileNames.LASTUPDATE + ".txt");
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
        bufferedWriter.write(lastUpdate);
        bufferedWriter.newLine();
        bufferedWriter.close();
        System.out.println("Make lastUpdate file.");
    }

    public ArrayList<String> makeUrlList(Document document) {
        ArrayList<String> urlList = new ArrayList<>();
        Elements elements = document.getElementsByClass("subtitle");
        for (Element element: elements) {
            Elements tmpElements = element.getElementsByTag("a");
            for (Element tmpElement : tmpElements) {
                String url = tmpElement.attr("href");
                String split[] = url.split("/");
                url = split[2];
                urlList.add(url);
            }
        }
        System.out.println("Make url list.");
        return urlList;
    }

    public void printNovel(String url, String ncode, Document document) throws IOException {
        String html = document.html();
        document = Jsoup.parse(html.replaceAll("(?i)<br[^>]*>", "/brton/"));
        File file = new File("./Novels/" + ncode + "/" + url + ".txt");
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
        Element element;
        Elements elements;
        List<String> lineList;
        element = document.getElementById("novel_p");
        if (element != null) {
            elements = element.select("p");
            lineList = elements.eachText();
            for (String line : lineList) {
                bufferedWriter.write(line.replaceAll("/brton/", "\n"));
                bufferedWriter.newLine();
            }
            bufferedWriter.write("------------------");
            bufferedWriter.newLine();
            bufferedWriter.newLine();
        }
        elements = document.getElementsByClass("novel_subtitle");
        bufferedWriter.write(elements.text());
        bufferedWriter.newLine();
        bufferedWriter.newLine();
        element = document.getElementById("novel_honbun");
        elements = element.select("p");
        lineList = elements.eachText();
        for (String line: lineList) {
            bufferedWriter.write(line.replaceAll("/brton/", "\n"));
            bufferedWriter.newLine();
        }
        element = document.getElementById("novel_a");
        if (element != null) {
            elements = element.select("p");
            lineList = elements.eachText();
            bufferedWriter.write("------------------");
            bufferedWriter.newLine();
            bufferedWriter.newLine();
            for (String line : lineList) {
                bufferedWriter.write(line.replaceAll("/brton/", "\n"));
                bufferedWriter.newLine();
            }
        }
        bufferedWriter.close();
        System.out.println("Make novel text file.");
    }

    void addTitles() throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("./Novels/titles.txt", true));
        for (DLData dlData: dlList) {
            String title = dlData.getTitle();
            String url = dlData.getUrl();
            String split[] = url.split("/");
            String ncode = split[3];
            bufferedWriter.write(title + "/,/" + ncode);
            bufferedWriter.newLine();
        }
        bufferedWriter.close();
        System.out.println("Title list was updated.");
    }
}
