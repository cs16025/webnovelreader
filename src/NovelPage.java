import javafx.event.Event;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class NovelPage {
    private Stage novelStage;
    private Scene scene;
    private SplitPane splitPane;
    private ToolBar topToolBar, bottomToolBar;
    private TextArea textArea;
    private Button prevButton, nextButton, darkModeButton, fontEnlageButton, fontEnsmallButton;
    private Pane pane;

    private File file;
    private FileReader fileReader;
    private String title;
    private int titleNum;
    private String line;
    private ArrayList<String> subTitleList;
    private String ncode;
    private ArrayList<String> fileList;
    private NovelProperty novelProperty;

    public NovelPage(String title, String ncode, ArrayList<String> subTitleList, ArrayList<String> fileList, NovelProperty novelProperty) {
        file = null;
        fileReader = null;
        this.title = title;
        this.subTitleList = subTitleList;
        titleNum = subTitleList.indexOf(this.title);
        line = null;
        this.ncode = ncode;
        this.fileList = fileList;
        this.novelProperty = novelProperty;
    }

    public NovelProperty showNewWindow() throws IOException {
        line = getNovel(titleNum);
        prevButton = new Button("←");
        prevButton.setOnAction(event -> {
            try {
                prevButtonAction();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        nextButton = new Button("→");
        nextButton.setOnAction(event -> {
            try {
                nextButtonAction();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        pane = new Pane();
        HBox.setHgrow(pane, Priority.ALWAYS);
        darkModeButton = new Button("ナイトモード");
        darkModeButton.setOnAction(event -> darkModeButtonAction());
        fontEnlageButton = new Button("Aあ");
        fontEnlageButton.setFont(new Font(11));
        fontEnlageButton.setOnAction(event -> fontEnlageButtonAction());
        fontEnsmallButton = new Button("Aあ");
        fontEnsmallButton.setFont(new Font(9));
        fontEnsmallButton.setOnAction(event -> fontEnsmallButtonAction());
        topToolBar = new ToolBar(prevButton, nextButton, pane, fontEnsmallButton, fontEnlageButton, darkModeButton);
        textArea = new TextArea();
        textArea.appendText(line);
        textArea.setWrapText(true);
        textArea.setScrollTop(Double.MAX_VALUE);
        textArea.setEditable(false);
        textArea.setFont(new Font(novelProperty.getFontSize()));
        splitPane = new SplitPane(topToolBar, textArea);
        splitPane.setOrientation(Orientation.VERTICAL);
        splitPane.setPrefHeight(750);
        splitPane.setPrefWidth(1000);
        scene = new Scene(splitPane);
        setColorMode();
        novelStage = new Stage();
        novelStage.setScene(scene);
        novelStage.setTitle(title);
        System.out.println("Show <" + title + ">.");
        novelStage.showAndWait();
        return novelProperty;
    }

    public void prevButtonAction() throws IOException {
        if (titleNum > 0) {
            line = getNovel(--titleNum);
            textArea.setText(line);
            title = subTitleList.get(titleNum);
            novelStage.setTitle(title);
            System.out.println("Show <" + title + ">.");
        } else {
            System.out.println("This is first page.");
        }
    }

    public void nextButtonAction() throws IOException {
        if (titleNum < subTitleList.size()) {
            line = getNovel(++titleNum);
            textArea.setText(line);
            title = subTitleList.get(titleNum);
            novelStage.setTitle(title);
            System.out.println("Show <" + title + ">.");
        } else {
            System.out.println("This is latest page.");
        }
    }

    public void darkModeButtonAction() {
        novelProperty.setNightMode(!novelProperty.getNightMode());
        removeColorMode();
    }

    public void removeColorMode() {
        if (novelProperty.getNightMode()) {
            scene.getStylesheets().remove(scene.getStylesheets().indexOf("DayMode.css"));
            System.out.println("remove DayMode.");
        } else {
            scene.getStylesheets().remove(scene.getStylesheets().indexOf("NightMode.css"));
            System.out.println("remove NightMode.");
        }
        setColorMode();
    }

    public void setColorMode() {
        if (novelProperty.getNightMode()) {
            scene.getStylesheets().add("NightMode.css");
            System.out.println("set NightMode.");
        } else {
            scene.getStylesheets().add("DayMode.css");
            System.out.println("set DayMode.");
        }
    }

    public void fontEnlageButtonAction() {
        int tmp = novelProperty.getFontSize();
        textArea.setFont(new Font(++tmp));
        novelProperty.setFontSize(tmp);
    }

    public void fontEnsmallButtonAction() {
        int tmp = novelProperty.getFontSize();
        textArea.setFont(new Font(--tmp));
        novelProperty.setFontSize(tmp);
    }

    public String getNovel(int num) throws IOException {
        file = new File("./Novels/" + ncode + "/" + fileList.get(num) + ".txt");
        fileReader = new FileReader(file);
        int tmp;
        StringBuilder result = new StringBuilder();
        result.setLength(0);
        while ((tmp = fileReader.read()) != -1) {
            result.append((char) tmp);
        }
        return result.toString();
    }
}
