import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import java.io.IOException;

public class TopPageController {
    @FXML
    public void searchButtonAction(ActionEvent event) throws IOException {
        App.getInstance().setPage("SearchPage.fxml");
        System.out.println("Transition search page.");
    }

    @FXML
    public void readButtonAction(ActionEvent event) throws IOException {
        App.getInstance().setPage("ReadingPage.fxml");
        System.out.println("Transition read page.");
    }
}
