import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.web.WebHistory;
import javafx.scene.web.WebView;

import java.io.IOException;

public class SearchPageController {
    private String url = "https://yomou.syosetu.com/";
    private String downroadTitle;
    private String downloadUrl;

    private DLModel dlModel;

    @FXML
    private WebView searchWebView;

    @FXML
    private TextField searchUrl;

    @FXML
    void initialize() {
        dlModel = new DLModel();
        searchWebView.getEngine().load(url);
    }

    @FXML
    public void backButtonAction(ActionEvent event) {
        WebHistory webHistory = searchWebView.getEngine().getHistory();
        if (webHistory.currentIndexProperty().get() == 0) {
            System.out.println("This is first page.");
        } else {
        webHistory.go(-1);
            System.out.println("Back to previous page.");
        }
    }

    @FXML
    public void tempDLButtonAction(ActionEvent event) {
        downroadTitle = searchWebView.getEngine().getTitle();
        downloadUrl = searchWebView.getEngine().getLocation();
        System.out.println("Add downloadList download: " + downroadTitle + ", url: " + downloadUrl);
        dlModel.addIndex(downroadTitle, downloadUrl);
    }

    @FXML
    public void downloadButtonAction(ActionEvent event) throws IOException {
        dlModel.format();
    }

    @FXML
    public void backToTopButtonAction(ActionEvent event) throws IOException {
        App.getInstance().setPage("TopPage.fxml");
        System.out.println("Back to top page.");
    }

    @FXML
    public void searchButtonAction(ActionEvent event) {
        if (searchUrl != null) {
            String jampToUrl = searchUrl.getText();
            searchWebView.getEngine().load(jampToUrl);
            System.out.println("Jamp to " + jampToUrl);
        } else {
            System.err.println("You cannot search.");
        }
    }
}
